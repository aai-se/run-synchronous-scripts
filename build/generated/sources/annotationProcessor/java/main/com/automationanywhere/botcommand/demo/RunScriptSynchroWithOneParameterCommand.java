package com.automationanywhere.botcommand.demo;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class RunScriptSynchroWithOneParameterCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(RunScriptSynchroWithOneParameterCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    RunScriptSynchroWithOneParameter command = new RunScriptSynchroWithOneParameter();

    if(parameters.get("ScriptPath") == null || parameters.get("ScriptPath").get() == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","ScriptPath"));
    }

    if(parameters.get("parameterList") == null || parameters.get("parameterList").get() == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","parameterList"));
    }

    if(parameters.get("InterpreterName") != null && parameters.get("InterpreterName").get() != null && !(parameters.get("InterpreterName").get() instanceof String)) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","InterpreterName", "String", parameters.get("InterpreterName").get().getClass().getSimpleName()));
    }
    if(parameters.get("ScriptPath") != null && parameters.get("ScriptPath").get() != null && !(parameters.get("ScriptPath").get() instanceof String)) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","ScriptPath", "String", parameters.get("ScriptPath").get().getClass().getSimpleName()));
    }
    if(parameters.get("parameterList") != null && parameters.get("parameterList").get() != null && !(parameters.get("parameterList").get() instanceof ArrayList)) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","parameterList", "ArrayList<Value>", parameters.get("parameterList").get().getClass().getSimpleName()));
    }
    try {
      Optional<Value> result =  Optional.ofNullable(command.action(parameters.get("InterpreterName") != null ? (String)parameters.get("InterpreterName").get() : (String)null ,parameters.get("ScriptPath") != null ? (String)parameters.get("ScriptPath").get() : (String)null ,parameters.get("parameterList") != null ? (ArrayList<Value>)parameters.get("parameterList").get() : (ArrayList<Value>)null ));
      logger.traceExit(result);
      return result;
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
