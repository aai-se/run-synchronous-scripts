/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.DICTIONARY;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

/**
 * @author Bren Sapience
 *
 */
@BotCommand
@CommandPkg(label="Run Script Synchronously", name="Run Script Synchronously", description="Run Script Synchronously and Catch Return Code and Output", icon="pkg.svg",
		node_label="Run Script {{scriptPath}}",
		return_type=DICTIONARY, return_label="Dictionary contains 2 keys: 'returncode', 'output' & 'error'", return_required=true)

public class RunScriptSynchroWithOneParameter {

	private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

	@Execute
	public DictionaryValue action(
			@Idx(index = "1", type = TEXT) @Pkg(label = "Interpreter")  String InterpreterName,
			@Idx(index = "2", type = TEXT) @Pkg(label = "Script Path & Name") @NotEmpty String ScriptPath,
			//@Idx(index = "2", type = TEXT) @Pkg(label = "parameters") @NotEmpty String ScriptParameter
			@Idx(index = "3", type = AttributeType.LIST)
			@Pkg(label = "List of Script Parameters", default_value_type = DataType.LIST)
			@NotEmpty ArrayList<Value> parameterList
	) {

		if ("".equals(ScriptPath.trim()))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "scriptPath"));

		String KEYRETCODE = "returncode";
		String KEYSTDOUT = "output";
		String KEYSTDERR = "error";
		Map<String, Value> ResMap = new LinkedHashMap();


		String OUTPUT = "";
		List<String> list = new ArrayList<String>();
		if(!InterpreterName.equals("")){
			list.add(InterpreterName);
		}
		list.add(ScriptPath);
		for(int i=0;i<parameterList.size();i++){
			list.add(parameterList.get(i).get().toString());
		}

		ProcessBuilder processBuilder = new ProcessBuilder(list);

		//String DEBUGOUT = "";
		/**
		for(int i=0;i<list.size();i++){
			String el = list.get(i);
			if(DEBUGOUT.equals("")){
				DEBUGOUT = el;
			}else{
				DEBUGOUT = DEBUGOUT + " " + el;
			}
		}
		 **/
		//System.out.println("CLI: "+DEBUGOUT);

		try {

			Process process = processBuilder.start();

			StringBuilder error = new StringBuilder();
			BufferedReader readerErr = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			String ERRORPARSED = readerErr.lines().collect(Collectors.joining()).replaceAll("\"","");

			StringBuilder output = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String OUTPUTPARSED = reader.lines().collect(Collectors.joining()).replaceAll("\"","");

			Value OUTPUTVAL = new StringValue(OUTPUTPARSED);
			Value ERRORVAL = new StringValue(ERRORPARSED);

			ResMap.put(KEYSTDOUT,OUTPUTVAL);
			ResMap.put(KEYSTDERR,ERRORVAL);

			int exitVal = process.waitFor();
			process.destroy();

			Value RETURNCODEVAL = new StringValue(Integer.toString(exitVal));
			ResMap.put(KEYRETCODE,RETURNCODEVAL);
			return new DictionaryValue(ResMap);

		} catch (IOException e) {
			//e.printStackTrace();
			ResMap.put(KEYRETCODE,new StringValue(Integer.toString(-1)));
			ResMap.put(KEYSTDOUT,new StringValue(""));
			ResMap.put(KEYSTDERR,new StringValue(e.getMessage()));
			return new DictionaryValue(ResMap);

		} catch (InterruptedException e) {
			ResMap.put(KEYRETCODE,new StringValue(Integer.toString(-1)));
			ResMap.put(KEYSTDOUT,new StringValue(""));
			ResMap.put(KEYSTDERR,new StringValue(e.getMessage()));
			return new DictionaryValue(ResMap);
		}

	}
	public static boolean isAlive(Process p) {
		try {
			p.exitValue();
			return false;
		}
		catch (IllegalThreadStateException e) {
			return true;
		}
	}
}
