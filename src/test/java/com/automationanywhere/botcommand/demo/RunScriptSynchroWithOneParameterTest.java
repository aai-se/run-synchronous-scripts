package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Map;

import static org.testng.Assert.assertEquals;

public class RunScriptSynchroWithOneParameterTest {

    RunScriptSynchroWithOneParameter command = new RunScriptSynchroWithOneParameter();

    @DataProvider(name = "scriptcalls")
    public Object[][] dataTobeTested(){

        ArrayList<Value> input1 = new ArrayList<Value>();
        input1.add(new StringValue("linus"));

        ArrayList<Value> input2 = new ArrayList<Value>();
        input2.add(new StringValue("linus"));
        input2.add(new StringValue("123"));

        ArrayList<Value> input3 = new ArrayList<Value>();
        input3.add(new StringValue("linus"));
        input3.add(new StringValue("123"));

        ArrayList<Value> input4 = new ArrayList<Value>();
        input4.add(new StringValue("-Scan"));
        input4.add(new StringValue("-Scantype"));
        input4.add(new StringValue("3"));
        input4.add(new StringValue("-File"));
        input4.add(new StringValue("\"C:\\mypet.txt\""));
       // input4.add(new StringValue("-File \"C:\\iqbot\\Document Samples\\split\\Sample_Invoice_Combined_BlankPages.pdf\""));

        return new Object[][]{
                {"","c:\\test.bat",input1,"hi there, linus. also: ","0"},
                {"","c:\\test.bat",input2,"hi there, linus. also: 123","0"},
                {"python","c:\\test.py",input3,"Let's start:Number of args:3args:['c:\\\\test.py', 'linus', '123']","0"},
                {"","C:\\Program Files\\Windows Defender\\MpCmdRun.exe",input4,"Scan starting...Scan finished.Scanning C:\\mypet.txt found no threats.","0"}
                //"Myvar 1 | my var 2 | abc | 123"
        };
    }

    @Test(dataProvider = "scriptcalls")
    public void imageBlurTests(String InterpreterName, String scriptPath,  ArrayList<Value> params, String ExpectedOutput,String ExpectedRetCode){
        DictionaryValue d = command.action(InterpreterName,scriptPath,params);
        Map<String,Value> map = d.get();
        String myRetcode = map.get("returncode").toString();
        String myOutput = map.get("output").toString();
        String myError = map.get("error").toString();
        System.out.println("DEBUG Return Code: "+myRetcode);
        System.out.println("DEBUG Output: "+myOutput);
        System.out.println("DEBUG Error: "+myError);
        assertEquals(myRetcode,ExpectedRetCode );
        assertEquals(myOutput,ExpectedOutput );
    }
}
